# Bachelorarbeit Meeting 01-04-2021

I read through your proposal again: https://zabka.it/posts/introduction/ - I guess before we can do a deepdive we should talk about it high level again:

## How does the JS Developer opt-into using the service

1. Use non-release build
2. Enable pref
3. Register config via WebExtension API

## How do we instrument the browser?

Unclear

1. You suggested `Element::GetAttribute` and `Element::GetInnerHTML`
2. Ehsan suggested generating code into the binding layer
3. Spidermonkey?

Can we catch calls to stuff like `Document.cookies` or `WebGLRenderingContext.readPixels` with 1.
Can I do 2. without slowing the browser to a crawl.
Who could guide me on 3. Ask baku!

PrintDumpJSStack https://searchfox.org/mozilla-central/source/storage/mozStorageConnection.cpp#1069
in Document Set Cookie

c) should we really do that in rust or re-use lots of C++ infrastructure we already have

We should do it in C++, I need to port my sample implementation over.
I've just never written C++ before and it kinda scares me.

Christophs Tipp:

- Jeden Tag 45 min schreiben.
- Security & Privacy Workshop Short Paper
