# Dorian Meeting notes <!-- omit in toc -->

## 2022-05-10

Points to adress:

- Keine Text-zu-Text transformation
- Hoehere Abstraktionsebene - Naja, eher niedriger in den Implentierungsschichten
- Keine allgemeine Aussage moeglich, da der Test-Case zu klein gehalten ist
- Keine Formalisierung der Forschungsfrage
- Keine Beachtung des aktuellen Stands der Forschung - Andere Implentierungen genauer analysieren??
- Einfluss der Entwurfsentscheidungen auf die Qualität des Forschungsprotoypen bleibt undiskutiert
- Die Darstellung in der Diskusion ist ebenfalls vage.

Sollte ich begruenden, dass ich mich auf diese beiden implentierungen beschraenkt habe, weil
ich die eine durch die andere Ersetzen wollte? Oder soll ich noch andere Systeme in den Vergleich aufnehmen?

Was ist eine gut formalisierte Forschungsfrage?

## 2022-05-03

- Gutachten lesen
- Pruefungsbuero??
- Gutachten lesen -> Darauf eingehen
- Fragen beantworten
- 30 min Vortrag
- Ergebnisse erklaeren - Arbeitsaufbau folgen
- Eins der Interessanten Probleme beschreiben
- Beispiel war zu einfach

english Slides

- Folien begleiten den Vortrag
- Maximal Stichpunkte
- Eine Folie pro Minute max
- Graphiken ersetzt so viel Text wie du zum erstellen der Graphik gebraucht hast
- Sequenzdiagramme rein
- Foliennummern / gesamtfoliennummern (fuer Referenz)
- Helles Design

Deutscher Vortrag

Bei Urteil warten bis man das Wort bekommt - Moderierte Diskussion

## 2022-02-04

- 2.2. Wofuer brauchen wir das instrument - Dadurch schwierig zu bewerten
- Kurz und praezise
- Code in section 2.1

## 2022-01-21

- Semantik definieren
- Nur eine layer graphik
- Nicht zu klein werden ?

## 2022-01-07

- Graphiken fuer unterschiedliche Injektion Szenarios und Actor Layout erstellt
- Einleitung mehrfach neu geschrieben
- Viel Erholt

- Was wuerde ich gerne im Paper lesen

## 2021-12-17

- Bildunterschrift sollte freistehen
  - Sagen was man sieht Welcher Prozess
- Im text bewerten und einordnen
- Backlink nice too have

## 2021-12-03

- Browser Aufbau in Kapitel 2
- Locality of Reference - Alles zusammen abhandeln
- Beispiel was im Alten kaputt geht für Kapitel 4
- Kapitel 4: OpenWPM vs CallMonitor
- Kapitel 4 -> Kapitel 5
- 20-30 Seiten als Ziel
- Abstract schreiben

## 2021-11-21

- Graphiken verbessert
- WebExtension erklärt
- Kapitel zur aktuell existierenden Instrumentierung geschrieben
- Tip: Print from browser +  PDF Crop
- Statt defensiv werden -> Das ist halt ein PoC

## 2021-10-06

- Nicht viel geschafft
- Job-Wechsel
- Trotzdem jetzt anmelden, damit ich Druck bekomme
- Das Layer Kapitel ausformuliert
- Sequenzdiagramm -> Env Process (vielleicht mehrere)

## 2021-09-21

- Graphiken fertig
  - Erweitert > SVG Text als Text exportieren
- Evaluation fertig
- Motivation nennen aber nicht ausführlich begründen
  - Privacy
  - WebCompat
  - Priorisieren
- Child Process~es~
  - Prosa oder Diagramm erweitern

## 2021-07-14

1. Schreiben ist schwer
2. Mehr Arbeiten mit TODOs
3. Weniger Prokrastinieren
4. Stichpunkte als Zwischenebene
5. Morgens Stichpunkte – Abends Refinement

## 2021-06-30

1. Anfangen mit dem Schreiben
   1. Biblatex
   2. Institutsvorlage und Vorlage einer Kommilitonen integriert
   3. Schreiben ist überwältigend
   4. TODO Struktur
2. Kein Implementierungsfortschritt
   1. Vielleicht config checking einfach im Parent Process -> keine Serialisierung & Synchronisierung

## 2021-06-16

1. Serialisierung
   1. TinyXML?
   2. JSON
   3. TOML? INI?
2. Andere Richtung funktioniert
3. Logging
4. TeX Vorlage auf Institutswebsite gucken
   1. https://www.informatik.hu-berlin.de/de/studium/formulare/vorlagen

## 2021-06-02

Status:

1. Fehler debuggt -> Call war wirklich zu früh
2. Lösung ist kompliziert -> mir wurde ein Hack empfohlen
3. Letzten zwei Wochen ein bisschen verzögert, weil mein Chef gekündigt hat
4. Bin jetzt quasi nur noch in der FF Codebasis unterwegs, sollte also schneller werden

ToDos:

1. Config Distribution Blogpost
    1. Actor vs Shoving into pref
    2. Eager vs. Lazy
2. Config Distribution via pref
    1. Serialisieren von C++ struct
3. Logging & return types
4. Test submitEvent ohne config check <- Das reicht vielleicht schon für die BA

## Bachelorarbeit Meeting 19-05-2021

Status:

1. Ich habe einen Test. Er schlägt fehl.
2. Ich überarbeite meine Blog-Posts, damit Leute Fragen stellen können
3. Ich muss mein aktuelles Problem und dessen Lösung aufschreiben, damit ich es in der BA
   verarbeiten kann.

ToDos:

1. Blog Posts mit aktuellem Wissen aktualisieren.
   1. Alles zum Thema Rust entfernen.
   2. WebApiInstrumentation durch CallMonitor ersetzen
2. Debugging Umgebung für Firefox aufsetzen
3. Meetings mit meinen Mentoren ansetzen.
4. Code umschreiben um den Pattern von Mozilla zu folgen
   1. Dokumentieren des Coding styles
5. Init call invertieren
   1. Aktuellen Zustand dokumentieren
   2. Logging aktivieren

Tipps:

- Screenshots vom Debugging

## Bachelorarbeit Meeting 24-03-2021

Viel zu IPC belesen
Blogpost dazu ist in Arbeit

## Bachelorarbeit Meeting 10-03-2021

Umzug hat viel Zeit in Anspruch genommen.
Erste Schritte in Mozilla-Central (dem Firefox Code Repo) gemacht
Ab Ende dieser Woche volle Konzentration auf BA

## Bachelorarbeit Meeting 24-02-2021

## Status

- Wenig geschafft, weil Umzug
- Mit Semesterende und Umzug geschafft mehr Zeit für BA
- Einen Blogbost geupdated

## Message Type

- Enum
- Objekt Orientierung (dyn trait)
- Einfach nur ein String


## 2021-02-10

### Arbeitsstruktur

- Immer geradelining darstellen
- Target Audience
  - Instrumentieren von anderen Sachen in Firefox (nein)
  - Andere Browserhersteller die Browser instrumentieren wollen
    - Am Beispiel von Firefox
  - Rust für COM?
- Überschriften
  - Motivation
    - Es ist schlecht weil
      - zu langsam für FingerprintingJS
    - Inspect
  - Problemstellung
  - Prior Work
Was will ich machen
  - Design der Implementierung
    - Äbwägungen dokumentieren
    - Rust vs C++
    - Bindings dokumentieren
      - COM refenzieren
    - MemoryMap vs Serializierung vs IPC
    - Information über die JS Engine bekommen
      - Dazu reflektion
      - Alle anforderungen an die JSEngine dokumentieren
    - Grenzen der Methode
  - Was können wir damit machen
    - (vorläufige) Benchmark -> Nächstes Paper
    - FingerprintingJS zeigen
  - Ausblick
    - Ist das portabel?
    - Falls es zu langsam ist, warum?


Abstract wird am Ende geschrieben