function func(data) {
  let encoded_data = window.btoa(data);
  let decoded_data = window.atob(encoded_data);
  if (data !== decoded_data) {
    console.log("This is weird");
  }
}
function main() {
  func("abcd");
  let function_string = window.atob.toString();
  if (function_string === "function atob() {\n    [native code]\n}") {
    func("top secret evil activity");
  }
  document.getElementById(
    "output"
  ).innerText = `The screens coordinates are ${window.screen.width} x ${window.screen.height}`;
}
