"use strict";

const CallMonitorManager = Cc[
  "@mozilla.org/callmonitor/subscription-manager;1"
].createInstance(Ci.nsICallMonitorSubscriptionManager);

const events = [];

let id = CallMonitorManager.register(
  {
    eventHappened: (ev) => {
      events.push({ apiName: ev.apiName, arguments: ev.arguments });
    },
  },
  {
    items: [
      {
        apiName: "Atob",
      },
      {
        apiName: "Btoa",
      },
    ],
  }
);
