# Estimation of workload

| Task                               | Expected Time | Status | Comments                                                     |
| ---------------------------------- | ------------- | ------ | ------------------------------------------------------------ |
| Write demo                         | 2 Days        | Done   |                                                              |
| Figure out where component belongs | 1 day         | ToDo   | Needs to be discussed with senior FF contributors            |
| Write component                    | 5 days        | ToDo   | Write core Infrastructure and define interfaces in both ways |
| Integrate into native code         | 10 days       | ToDo   | Find common callsites and instrument them                    |
| Write tests                        | 3 days        | ToDo   | Make sure the extension is functional                        |
| Integrate into OpenWPM             | 2 days        | ToDo   | Rip out the pagescripts and instead call exposed API         |
