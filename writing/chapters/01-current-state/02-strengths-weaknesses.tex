\subsection{Strengths and Weaknesses}
\label{subsec:openwpm-issues}

The JavaScript instrument has some very clear strengths and less obvious weaknesses.

Its strengths lay in its unrestricted access to JavaScript's introspection
capabilities.
Being able to access information about the caller of a WebAPI via the call stack
and serialize the arguments using language level utilities is extremely powerful,
as it allows it to emit rich events, that capture a lot of metadata. This allows
researchers to later correlate these events and use them for complex analysis.

An example of a websites script and the respective captured event can be seen
in \autoref{fig:Example}
\pgfplotstableread[text indicator=",col sep=comma]{./tables/javascript.csv}{\jstable}
\begin{figure*}
  \begin{subfigure}{\textwidth}
    \begin{lstlisting}[language=JavaScript]
function func(data) {
  window.btoa(data);
}
function main() {
  func("abcd");
}\end{lstlisting}
  \end{subfigure}
  \begin{subfigure}{\textwidth}
    \begin{lstlisting}[language=JSON]
{
  "id" : 1,
  "incognito" : 0,
  "browser_id" : 1812454466,
  "visit_id" : 7003191432381221,
  "extension_session_uuid" : "ac42fb19-c56f-4b30-9025-c69a1572adbb",
  "event_ordinal" : 6,
  "page_scoped_event_ordinal" : 0,
  "window_id" : 1,
  "tab_id" : 1,
  "frame_id" : 0,
  "script_url" : "http:\/\/0.0.0.0:8000\/evil.html",
  "script_line" : "6",
  "script_col" : "33",
  "func_name" : "func",
  "script_loc_eval" : "",
  "document_url" : "http:\/\/0.0.0.0:8000\/evil.html",
  "top_level_url" : "http:\/\/0.0.0.0:8000\/evil.html",
  "call_stack" : "func@http:\/\/0.0.0.0:8000\/evil.html:6:33
                  main@http:\/\/0.0.0.0:8000\/evil.html:13:11",
  "symbol" : "window.btoa",
  "operation" : "call",
  "value" : "",
  "arguments" : "[\"abcd\"]",
  "time_stamp" : "2022-01-22T12:01:17.608Z"
}\end{lstlisting}
  \end{subfigure}
  \caption{A piece of JavaScript code and a JSON representation of the event captured by the JavaScript instrument when the \lstinline{window.btoa} function
  was called.\\
  The event is rich in metadata, capturing not only the arguments but also the exact caller location in the file and the callstack}
  \label{fig:Example}
\end{figure*}


Through WebExtension APIs exposed in the content script, OpenWPM is able
to record the window, tab and frame id. This makes it possible to attribute
the function call to a specific iFrame, an isolated document which websites can embed in
themselves but
don't control \autocite{mdn-iframe}. This information is crucial to understand, if
the call was made by the website visited, a so called first-party call, or by
another site that was embedded, which would be a third-party call.

In addition,
by analyzing the callstack, they can
observe the URL of all scripts involved in the call as well as the location of
each function in their respective files. This information is useful to determine,
the source of tracking scripts even when they are embedded in a first-party site.

However, the instrumentation also has one major vulnerability,
the wrapping code which is running as part of the website.
As it has disabled the Xray wrappers to have direct access to the WebAPIs,
the browser treats it as part of the website.
This allows a malicious website
to detect whether a given API is instrumented by calling
\lstinline{toString} on the function.
If it has been overwritten, \lstinline{toString} will return
the source code of the function, otherwise it will just return
\lstinline{[native code]}.
This detection vector is used in \autoref{sec:Evaluation}
to evade the JS instrument.

In addition to malicious detection, the overwritten function could
also be detected by benign, so-called polyfill scripts, that are used to
implement newer features to older browser, by checking if the function
exists and if it can not be detected, providing their own
JavaScript implementation for the feature. Since the wrapped functions
does not behave like a native implementation, the script might mistake
them for missing or outdated functions and overwrite them.
