\subsection{The Architecture}
\label{subsec:description}

OpenWPM uses a custom add-on, that cotains its data collection utilities,
which communicates with the execution platform, from where it receives
commands and to which it sends the collected data.

This add-on, as all modern add-ons, uses the WebExtension
standard, which will be described in this section.
Additionally, this section will explain how the JavaScript uses
the facilities provided by the WebExtension standard to record
WebAPI function calls.

\subsubsection{Architecture of a WebExtension}

Extensions, also called add-ons, can modify and enhance the users experience of a browser.
Common use cases include ad blockers, password managers or themes.
Extensions in Firefox use the standardized WebExtension APIs.
These APIs expose a wide variety of functionality to add-on developers, such as
the clipboard, the users history or multiple points at which the add-on developer
can intercept and manipulate requests made to the internet by websites
(before the request is made, after it has been received, etc.).
\autocite{mdn-webext}.

The standard also specifies
a packaging format to declaratively describe a WebExtension.
A WebExtension can contain the following artifacts:
\begin{itemize}
    \item manifest.json
    \item Background scripts
    \item Content scripts
    \item Icons
    \item Sidebars, popups, and options pages
    \item Web-accessible resources
\end{itemize}

The manifest.json is mandatory as it contains extension metadata,
including which other files are part of the extension and which APIs the add-on intends to use,
so the user can be asked for those specific permissions when installing the it \autocite{mdn-anatomy-of-webext}.

\paragraph{Background Scripts}

Background scripts maintain long-term state.
They have access to the full WebExtension APIs, if they have requested
the appropriate permissions.

E.g. background scripts can dynamically register new content scripts
or maintain a connection to external servers.

\paragraph{Content Scripts}

Content scripts have access to and can manipulate web pages.
They are loaded into the web page and run in the context of that page.

They have limited access to WebExtension APIs but have the ability
to send messages to the background script which can then interact
with the world on their behalf.

While content scripts are running in the context of the web page
and can access variables exposed on the document of the website,
they do not have the same view of the document as the JavaScript
of the website, instead they see the document through so called
Xray Wrappers.

\paragraph{Xray Wrappers}

To allow WebExtensions to safely access the contents of a web page, Firefox
always exposes the native version of a WebAPI to content scripts.
This means that even if e.g.
a website were to override a property on an HTML element
like so
\begin{lstlisting}[language=JavaScript]
    document.getElementById("testElement").matches =
     (s) => {return true;}
\end{lstlisting}
if the WebExtension were to call the \lstinline{matches} function on \lstinline{testElement},
it would still
call the original implementation.
This way the WebExtension can trust that it is able to safely navigate the DOM
or receive truthful information when querying for website state \autocite{fsd-xray}.

However, this practice also works the other way around, so any modifications
the content script does do its view of the document do not
get reflected back into the websites view.

This is problematic when instrumenting WebAPIs with the JavaScript instrument,
as the instrumentation code has to wrap the WebAPIs in order to capture calls
from the website before they reach the WebAPI.
The circumvention of this mechanism and its consequences are described in the next section.

\clearpage
\subsubsection{JavaScript Instrument}

\begin{figure}
    \centering
    \includegraphics[width=\columnwidth]{MessageCapture}
    \caption{An UML sequence diagram showing the process of injecting instrumentation code into
        the web page and the propagation of events captured by the JavaScript instrument
        to OpenWPM's execution platform (at the left)}
    \label{fig:OpenWPM}
\end{figure}

The JavaScript instrument is part of the add-on, that OpenWPM
installs in the Firefox browser before it visits a website,
to collect data during a visit. In particular, the JavaScript instrument allows the user to
specify a set of WebAPIs and whether they want to capture the callstack or the arguments
of a WebAPI function call.

\autoref{fig:OpenWPM} shows the process by which the JavaScript instrument
gets set up and sends information back to the aggregator (an external server
receiving and saving out captured data) once a WebAPI function gets called.

The diagram's left side should be understood as OpenWPM's automation platform,
as it issues commands to and receives information from the browser and the
add-on.
The diagram starts off after the browser has been started,
the extension has been installed,
and the platform
has detected that the background script is ready to receive information.

The first step is sending the config to the background script, which then
proceeds to generate an appropriate content script, which it registers with
the browser using WebExtension APIs. At this point the configuration is baked
into the script and can no longer be changed.

Once the platform is informed that the registration is complete, it instructs
the browser to visit a website. This results the browser creating a new
WebContent environment, loading the content,
injecting the previously registered content script
into the scope of the website and executing it.
This is the point where the content script needs to bypass the X-Ray wrappers to make sure
that it isn't wrapping its own WebAPIs but the
ones exposed to the web content. To do this the content script creates
a \lstinline{<script>} tag as the first element, letting it do the wrapping
and then removing it again.
As this happens before the web content starts executing, it is
undetectable for the website code.

Wrapping is done in two different ways depending what is being instrumented.
If it is a (nested) property of the global Window object, such as
\lstinline{window.screen.height}, the JavaScript instrument calls
\lstinline{Object.defineProperty} on the property to wrap
the property in a getter.
However, if the object being instrumented is one that does not exist by default,
such as the \lstinline{IntersectionObserver}, the JavaScript instrument
overrides its prototype's properties instead. This way, any object
that has \lstinline{IntersectionObserver} as its prototype, will
call instrumented methods.

As the wrapping code now is no longer considered part of the WebExtension code
it doesn't have to deal with X-Ray wrappers but also has no access to the
WebExtension APIs. To still communicate with the background script it
has to use the content script as a relay. The wrapping code emits a
custom event, which is propagated to the event handler of the content script,
which then uses the \lstinline{runtime.SendMessage} API to forward the message
to the background script.
To capture information about the caller of the API, the instrument creates a
JavaScript \lstinline{Error} object and accesses the Firefox-specific API
\lstinline{stack} which contains the callstack of an error as a string \autocite{mdn-error}.
It also captures the arguments array by serializing them before the WebAPI function call.

This process is repeated for every call to a WebAPI function that has been instrumented
until the platform receives a command to close the tab and orders the browser
to do so.
The browser then proceeds to destroy the tab containing the web content environment
and the content script.

