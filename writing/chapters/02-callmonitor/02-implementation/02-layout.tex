\subsubsection{CallMonitor's Architecture}

In this section I will present the current architecture of CallMonitor,
by first outlining the high level architecture and then
describing the implementation by following the process of registration and event capturing.

The current implementation is a proof of concept that only exposes its registration
interface to privileged JavaScript code in the main process. It captures events
for two WebAPI functions, namely \href{https://developer.mozilla.org/en-US/docs/Web/API/btoa}{\lstinline{btoa}} and
\href{https://developer.mozilla.org/en-US/docs/Web/API/atob}{\lstinline{atob}},
which en- and decode binary data to/from ASCII respectively.
The captured events contain the function argument, but no information about the caller or
any other context.

The code of the current implementation can be found under
\url{https://phabricator.services.mozilla.com/D110681}.

\begin{figure}
    \centering
    \includegraphics[width=\columnwidth]{ClassDiagram.pdf}
    \caption{A UML class diagram of all C++ classes in the CallMonitor component}
    \label{fig:ClassDiagram}
\end{figure}

\paragraph{High-Level}

CallMonitor has to interact with the rest of the Firefox codebase
at two points.
It has to collect data, when a WebAPI function call happens in a web content process
and inform consumers in the main process that an event they registered for has
occurred.

The interfaces for these two tasks are defined in \lstinline{nsICallMonitorEventManager}
and \lstinline{nsICallMonitorSubscriptionManager} respectively.

The rest of the component is designed around maintaining the state or
providing the connection between these two interfaces.

See \autoref{fig:ClassDiagram} for all classes involved.

\paragraph{Registration}

When another part of Firefox has acquired a reference to the \lstinline{CallMonitorManager}
through the CallMonitor service, it can register a subscriber with the CallMonitor component;
the callback and its associated config are then stored in the CallMonitorManager. It then
returns a unique id back to the caller, which allows the caller to unregister the callback
at a later time.

In the current implementation the CallMonitorManager does not
forward any information to the rest of the component, however other approaches
to configuration handling get discussed \autoref{subsec:Configuration}.

\paragraph{Event Capturing}

When an instrumented WebAPI function gets called, it acquires a reference to the
\lstinline{CallMonitorChild} by asking its manager (\lstinline{PContentChild}) to either
create it or return an existing reference.

It then calls the \lstinline{submitEvent} method, which in turn forwards the
event to the Main Process by calling \lstinline{SendEventHappened}, a message
defined in the \lstinline{PCallMonitor} protocol.
When the \lstinline{CallMonitorParent} receives the message, it acquires
a reference to the \lstinline{CallMonitorManager} and forwards the message.
The \lstinline{CallMonitorManager} then checks the subscribers and
invokes their callback, should they have registered for the specific
WebAPI function.
