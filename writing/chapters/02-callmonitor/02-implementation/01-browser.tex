\subsubsection{Context}

\paragraph{Multi Process Architecture}

All major browsers use a multi process architecture. This
means they run different tasks on different processes, allowing them
to use OS level security features to protect against malicious
web code \autocite{chromium-processes} \autocite{webkit-processes}.

Firefox, for example, uses a central process called main process,
to maintain browser state, such as history or the password manager
and other processes to deal with the different stages of processing a website,
such as loading the website in the socket process, parsing the HTML, CSS
and JavaScript and executing their JavaScript in a web content process or drawing them in the
render process \autocite{fsd-process-model}.

This separation of responsibility and defining static interfaces between
processes through actors, makes it possible to mitigate the impact
of a single exploit.
Should, for example, an attacker be able to achieve arbitrary code
execution on the network process, they will still be unable
to access cookies or draw to the screen.

The processes that are relevant in the context of this thesis
are

\begin{itemize}
    \item the web content process as the place, where CallMonitor collects
    the information about WebAPI function calls
    \item and the main process, where CallMonitor subscribers register to receive information
\end{itemize}

\paragraph{WebIDL}

WebIDL is an interface definition language, that is used by the
WHATWG to specify WebAPIs \autocite{whatwg-webidl}.
The IDL is a typed language that specifies standardized interfaces
and defines how these interfaces should be exposed to
ECMAScript.
By defining its semantics in terms of ECMAScript,
the IDL is able to achieve consistent behavior,
regarding things such as iteration order,
for different WebAPIs and across browsers.

Firefox consumes these IDL files to generate binding code.
This binding code gets called whenever a WebAPI function is invoked.
It unwraps the JavaScript arguments, if they exist,
checks if the invariants encoded in the IDL are upheld,
and calls the C++ implementation of the function.

This will be discussed further in \autoref{subsec:layers}.

\paragraph{Actors}

\begin{figure}
    \centering
    \includegraphics[page=1,width=\columnwidth]{FreshGraphics}
    \caption{A visualization aid showing the relationship between top level actors and their managees}
    \label{fig:Actors}
\end{figure}

To allow for secure communication across processes Firefox
uses an actor model.
In the traditional actor model, there is no global state
instead actors are independent entities that are only able to perform three types of actions,
changing their internal state, sending messages to
other actors or creating new actors \autocite{clinger1981foundations}.

However unlike the traditionally defined
actor semantics Firefox's actors
can not reach out to arbitrary other actors or create arbitrary
new actors.
Instead, actors always exist as pairs, where one side is defined
as a parent and the other as a child, and can only send statically defined
messages to their counterpart.
These actor pairs are split into two classes, top-level actors and
managed actors.
Top-level actor pairs initialize themselves by connecting with
their counterpart over a message pipe. This connection
is then
used to communicate between the two endpoints.
All processes in Firefox have a top-level actor pair to communicate
with the main process, this way the main process can coordinate
the different processes. The main process always acts as a parent
in these relationships.

Managed actors do not initialize themselves but instead
get created by their manager, when their manager receives a
request to create them. Since a managed actor doesn't have
its own message pipe, it has to go up the management
chain to the top-level actor to use its message pipe to reach
its counterpart on the other side.
As managed actors are subordinate to their manager,
they will be automatically shut down when their manager is.

\autoref{fig:Actors} exemplifies this by showing
a simplified actor setup for the relationship between
a content process and the main process.
When PCallMonitorChild wants to send a message
to its parent, it does not have a direct connection
but instead communicates with its counterpart
through the message pipe of the top-level actor,
which happens to be its direct manager.

Actor protocols in the Firefox code base are defined by IPDL,
the interprocess communication definition language.
Protocols have to use the prefix P. In the examplary figure
the top-level protocol is called PContent, which manages
the PExtensions and PCallMonitor protocols.
Each protocol defines which types of messages can be sent,
in which directions they can be sent and which values they contain.

\paragraph{XPCOM}

XPCOM, the Cross Platform Common Object Model, is a technology that allows
sharing objects between different programming languages. In the context of
Firefox XPCOM allows developers to write a component in a language of their choice
and expose it to JavaScript, C++ and Rust.

XPCOM interfaces are defined in XPIDL, the XPCOM interface definition language
and canonically are prefixed with nsI. XPIDL allows Firefox developers to
typed interfaces with functions and properties.
An object that implements an XPCOM interface is commonly referred to
as an XPCOM component.
References to XPCOM components can be acquired in a language-agnostic way
through XPCOM services.

The CallMonitor implementation uses XPCOM in two places.
On the event capturing side, \lstinline{nsICallMonitorEventManager} is defined
to be able to capture events in whatever language the WebAPI might be implemented in.
On the consumer side, \lstinline{nsICallMonitorSubscriptionManager} exposes
the subscriber interface to any language that consumers want to use.
Both of these interfaces will be described in more detail in the next section.
