\clearpage
\subsubsection{Configuration Management}
\label{subsec:Configuration}

To achieve feature parity with the JavaScript instrument, CallMonitor needs to allow
users to dynamically specify which WebAPIs they are interested in.
Since polling is generally discouraged in Firefox, as it always introduces a trade off between
resource overhead, when choosing a small polling interval, and latency, when choosing a big
polling interval, the interface had to be callback based.

As callbacks are a common pattern in the codebase, there also exist conventions around
how such a callback based interface should be designed.
The general pattern is, that the consumer of the API should
be able to register their callback and the associated metadata (in this case the configuration)
and in return receive a unique identifier, that allows them to refer to this registration.

This design allows for multiple consumers to register themselves with the same API without
interfering with one another, but it also complicates the internal state
of the CallMonitor compared to the JavaScript instrument. Instead of baking in one
configuration for a single consumer on startup, CallMonitor needs to maintain a
mapping between identifiers and their associated callback-metadata pair,
which can change at runtime, by consumers registering and unregistering themselves.

This left the following options:

\paragraph{Hard-coded Config}

As previously established, the WebAPIs get called in a different process
than the one the registration is happening in.
Specifying a fixed set of configurations at compile time
and allowing the user to only register
their callbacks for one of these configs, would sidestep the issue
of synchronizing the internal state across multiple processes,
especially since new web content processes get created for each new domain visited.

However, this would be a degradation in user experience compared
to the JavaScript as compiling Firefox is both too resource intensive
and too complex for privacy researchers, when they want to
instrument a custom set of WebAPIs.

As there still would be no communication to the web content processes
which configs currently have subscribers, they would still have to send all
messages for all configs.
A slight variation on this approach would be to require the callback code
to also be written at compile time, to avoid the last issue mentioned.

While this approach would have been the easiest to implement,
it breaks one of the core requirements, so it was discarded.

\paragraph{Centralized Config}
\begin{figure}
    \centering
    \includegraphics[width=\columnwidth]{configuration/CurrentFlow.pdf}
    \caption{An UML sequence diagram showing a single consumer registering with CallMonitor and then
     visiting a web page; the page makes a single WebAPI request, which gets unconditionally propagated to the main process due to the centralized config approach}
    \label{fig:Centralized}
\end{figure}

The approach I choose for the prototype was to keep the
configs and callbacks exclusively in the \lstinline{CallMonitorParent} and
not share it out to web content processes.

This approach allows exposing an API that could achieve
feature parity with the JavaScript instrument, while keeping
the implementation complexity low.

As seen in \autoref{fig:Centralized} the trade off for this approach
is that all events get send
to the main process, where the filtering happens.
As PCallMonitor is currently a managed protocol, that might
negatively impact the communication between the
web content process and the main process, but as the
proof of concept implementation only instrumented
a rarely used functions, there was no such impact.

\paragraph{Distributed Config}

A production component should not make unnecessary IPC calls especially since
a production-ready CallMonitor component would need to cover all WebAPIs,
which means a single traversal of the DOM could generate hundreds if not
thousands of events. As such web content processes must be able to decide locally whether
an event needs to be sent to the main process.

To be able to make this decision, each content process would need
to have at least the union of all active configs, but possibly better,
a mapping from registration token to config, so it can send a pair
of event and list of registration tokens to call via the \lstinline{EventHappened}
message.

\autoref{fig:Distributed} shows the added round trip of
propagating the config to an existing web content process when
a new consumer registers itself
as well as the added benefit of being able to make a local decision
in the content process.

What the figure hides is the complexity of keeping all web content processes
up to date. To achieve this, the main process would need to send the
content process the current configuration when the process starts and
then incrementally update the config every time it changes in the main process.
As running any web content in a process without having the configuration
initialize would result in missing events, CallMonitor would require
synchronous IPC in the process creation.
However, synchronous IPC is discouraged to the point that Firefox
developers need to convince the IPC to allowlist their call in a
special file.

Despite these implementation complexities, I still believe this is
the right approach for a high throughput system.

\begin{figure}
    \centering
    \includegraphics[width=\columnwidth]{configuration/ProperFlow.pdf}
    \caption{A UML sequence diagram showing a single consumer registering with CallMonitor and
     then visiting a web page; the page makes a single WebAPI request, which gets checked
     in the content process and only propagated if it needs to be due to the centralized config approach}
    \label{fig:Distributed}
\end{figure}
