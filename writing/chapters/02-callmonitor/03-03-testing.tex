\clearpage
\subsubsection{Testing}
\label{subsec:testing}

While different parts of the Firefox code base use a variety of testing
frameworks, such as GoogleTest \footnote{https://github.com/google/googletest},
the native Rust framework \footnote{https://doc.rust-lang.org/book/ch11-00-testing.html}
or xpcshell, a Mozilla internal framework to test XPCOM components \autocite{fsd-xpcshell}, the majority
of tests use mochitest, a JavaScript regression testing framework, that allows
testing all scriptable components of the Firefox codebase \autocite{mdn-archive-mochitest}.

Most mochitest, including mine, perform function testing as defined by Glenford J. Myers \autocite{MyersGlenfordJ.2012Taos},
as they do not test the module against the pure interface specification but rather test, that the component
under test meets the external specification.
This is necessary, as there are very few components which exist isolated in the Firefox code base.
Instead, it is common, that to perform a single task a component has to interact with countless
other systems.

All mochitest start a full browser which is then controlled via JavaScript running in different contexts.
These different execution contexts are called flavors.
There are four flavors of mochitest \autocite{Mochitest-Flavors}:
\begin{itemize}
    \item a11y — a11y or accessibility, is the practice of ensuring access to people with disabilities.
    All tests ensuring that the browser is usable for these people have the a11y flavor.
    \item plain — These tests run in content scope and only have access to a limited numbers of privileged
                  APIs, through the \lstinline{SpecialPowers} object.
    \item browser — Tests, that aim to test the browsers UI should use this flavor. They run in privileged scope.
    \item chrome — These tests are also run in a privileged mode, but aim to test the privileged APIs.
\end{itemize}

Originally, I assumed that the xpcshell framework would be sufficient for testing CallMonitor,
however as xpcshell tests don't create a full browser but instead only initialize
the XPCOM infrastructure, there was no way to submit events, as the necessary
interface is only exposed in the content process, which does not get created as part of these
tests.

Tests should only use as much enhanced access as required to make sure, the
real use case and the test use the same code paths.
As such, I evaluated the different flavors of mochitest against my testing requirements.
The a11y flavor was immediately discarded as CallMonitor is not an accessibility feature.
Plain mochitest run in the content process, which is insuffiecient to test the
component as being able to receive events in the main process is it core purpose.
Finally, as CallMonitor does not expose any UI, using the browser flavor was also out
of question.

This left the chrome flavor, which is a fit, as CallMonitor exposes a privileged
API in the main process.

So the test for the proof of concept works as following

\begin{enumerate}
    \item It registers with the \lstinline{CallMonitorManager} through the
         \lstinline{CallMonitorSubscriptionManager} interface
    \item It creates a new tab and points it to an HTML file that calls \lstinline{atob}
    \item It waits until it get notified, that the WebAPI function call has happened
    \item It assert that the event contains the expected argument string
\end{enumerate}

This setup tests the entirety of the current implementation.
For a more complete implementation, that also provides the WebExtension API, there would have to
be an additional test, which would need to register as an addon and use the API exposed there.

While this approach works very well for a proof of concept,
it would have significant problems at scale, as creating a new test file for each
WebAPI function wouldn't scale.

Testing every single instrumented API might not be required, once each API instrumentation is no longer handwritten,
since a single error in the generated code would affect all paths.

However, to test a representative number of APIs to ensure, things are working as expected at scale, it could be reasonable
to set up a channel between the web content and priviliged test context and pass a string
to it, on which the web content could then call \lstinline{eval}, which to the instrumentation would be indistinguishable
from having the code be written in the file beforehand, except for perhaps the callstack.
As there are only two functions instrumented in the current implementation,
this approach seemed overly complex.
