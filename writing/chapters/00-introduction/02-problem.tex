\subsection{Problem statement}

It is imperative that
Web privacy researchers are able to observe how a website
uses JavaScript to interact with the browser, to detect
whether it is fingerprinting the user and if so, which techniques it uses.

As JavaScript needs to use the WebAPIs to affect or observe
state outside the interpreter (see \autoref{fig:JSInteraction}) and browser fingerprinting requires
information outside the interpreter, it is sufficient for the researchers to be able to
observe all calls through the WebAPIs to detect whether
a website is engaging in malicious activities.
Unfortunately there are no standardized interfaces for observing WebAPI function calls.
This leaves researchers with two options:
\begin{enumerate}
    \item Change the websites code to record calls before they reach the WebAPI.
    \item Change the browser to record WebAPI function calls after they have been made.
\end{enumerate}

Current research uses both of these approaches, however most commonly they only
modify the browser in such a way that it answers their specific research question \autocite{5504803}
\autocite{arshad2016include}.
This lack of reusability leads to a lot of duplicated effort and
hinders combining and comparing the collected data.
To mitigate this, a number of open source browser-based web crawlers
have been published in the last years, such as DuckDuckGo's Tracker Radar Collector \autocite{gh-ddg-crawler} or Kraaler \autocite{panum2019kraaler}.

One of the most commonly used crawlers is OpenWPM, a modular web crawler written in Python
that uses the Firefox browser and the Selenium automation framework to crawl the web \autocite{englehardt2016census}.
It provides facilities to detect a wide range of website activities as well as
a platform that allows for parallelism and abstracts data storage.
One of OpenWPM's tools for data collection is the JavaScript instrument which injects scripts into the
website to record WebAPI function calls.

Due to the popularity of Chromium-based browsers (such as Google Chrome or Microsoft Edge),
many browser-based crawlers (including the Tracker Radar Collector and Kraaler) use Chromium for data collection.
For crawls using Firefox, OpenWPM is the leading option, but as it changes the website
code to intercept calls to the WebAPI functions, it is inherently limited in performance
and detectability by having to use JavaScript.

The lack of a browser-integrated, general purpose instrumentation tool
that uses the Firefox browser will be addressed in this thesis.
This tool should allow researchers to instrument arbitrary WebAPI function calls with sufficient
metadata to be able to advance the state of web privacy research while being
indistinguishable from an uninstrumented browser and ensure that web privacy research doesn't only rely on data
collected for a single engine.

This goal is achieved, if CallMonitor captures the same width of information as the
JavaScript instrument currently does, without CallMonitor exhibiting the same shortcomings as
the JavaScript based implementation.